import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import GlobalStyle from './theme/css/GlobalStyle';
import Login from './components/authentication/Login';
import ForgotPassword from './components/authentication/ForgotPassword';
import ResetPassword from './components/authentication/ResetPassword';
import ResetLinkSent from './components/authentication/ResetLinkSent';
import Dashboard from './components/Dashboard';
import {
    PATH_FORGOT_PASSWORD,
    PATH_HOME,
    PATH_LOGIN,
    PATH_RESERT_PASSWORD,
    PATH_RESET_LINK_SEND,
} from './components/constants/routeConstant';
import ProtectedRoute from './components/ProtectedRoute';

function App() {
    return (
        <>
            <GlobalStyle />
            <div className="sharebuck-app">
                <BrowserRouter>
                    <div id="site-content">
                        <Switch>
                            <Route exact path={PATH_LOGIN} component={Login} />
                            <Route exact path={PATH_FORGOT_PASSWORD} component={ForgotPassword} />
                            <Route exact path={PATH_RESERT_PASSWORD} component={ResetPassword} />
                            <Route exact path={PATH_RESET_LINK_SEND} component={ResetLinkSent} />
                            <ProtectedRoute exact path={PATH_HOME} component={Dashboard} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </div>
        </>
    );
}

export default App;
