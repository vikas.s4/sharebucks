const Theme = {
    // color Declaration
    $baseColor: '#001d1b',
    $secondaryColor: '#266a65',
    $lightBlackColor: '#888cbc',
    $BlackColorshade: '#535353',
    $TextColorOne: '#12413e',
    $BrownColor: '#8e4e4e',
    $colorSkyblue: '#007bff',
    $colorGreenShade: '#2e817b',
    $whiteColor: '#ffffff',
    $whiteSmoke: '#f7f7f7',

    // font Size
    $smallFont: '14px',
    $regularFont: '16px;',
    $extraLargeFont: '52px',
    $LargeFont: '40px',
    $MediumFont: '24px',
    $smallfont: '20px',

    // Media Query
    $mobileS: `(min-width: 320px)`,
    $mobileM: `(min-width: 375px)`,
    $mobileMax: `(max-width: 767px)`,
    $tablet: `(min-width: 768px)`,
    $desktop: `(min-width: 992px)`,
    $desktopLarge: `(min-width: 1200px)`,
    $desktopExtraL: `(min-width: 1600px)`,
};

export default Theme;
