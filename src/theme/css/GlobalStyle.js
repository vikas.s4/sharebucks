import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

    body{
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        width: 100%;
    }
    ul{
        margin:0;
        padding:0;
    }
    ul li{
        list-style-type:none;
    }
    a:hover{
        text-decoration:none;
    }
    img{
        max-width:100%;
        height: auto;
    } 
    button:focus{
        outline:none;
        border:none;
    }        
`;
export default GlobalStyle;
