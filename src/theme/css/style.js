import styled, { css } from 'styled-components';
import Theme from './Theme';
import happyPeopleBg from '../images/happy-people-bg.jpg';

// global bg
export const FormError = styled.div`
    color: red;
`;
const globalBg = css`
    background: ${Theme.$secondaryColor};
    color: ${Theme.$whiteColor};

    &:hover {
        background: ${Theme.$colorSkyblue};
        color: ${Theme.$whiteColor};
    }
`;
// Background Image Style Declaration
const BgImgstyle = css`
    background-size: cover !important;
    background-position: center center !important;
    background-repeat: no-repeat !important;
`;

export const MyRow = styled.div`
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
`;

const Col12 = css`
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
`;

const Col8 = css`
    -ms-flex: 0 0 66.666667%;
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
`;
const Col4 = css`
    -ms-flex: 0 0 33.333333%;
    flex: 0 0 33.333333%;
    max-width: 33.333333%;
`;
export const LoginImgcolumn = styled.div`
    @media ${Theme.$mobileS} {
        ${Col12}
    }
    @media ${Theme.$tablet} {
        ${Col8}
    }
`;
// Heading Declaration

const MegaHeading = css`
    @media ${Theme.$mobileS} {
        font-size: ${Theme.$LargeFont};
        line-height: 62px;
    }

    @media ${Theme.$desktop} {
        font-size: ${Theme.$extraLargeFont};
        line-height: 48px;
    }
`;
const smallHeading = css`
    font-size: ${Theme.$smallFont};
    line-height: 24px;
`;
const mediumHeading = css`
    font-size: ${Theme.$MediumFont};
    line-height: 28px;
`;
const smallText = css`
    font-size: ${Theme.$smallFont};
    line-height: 17px;
`;
export const HeadingH1 = styled.h1`
    ${MegaHeading};
    color: ${(props) => (props.color ? props.color : '#001d1b')};
`;
export const HeadingH4 = styled.h4`
    ${mediumHeading};
    color: ${(props) => (props.color ? props.color : '#001d1b')};
`;
export const HeadingH5 = styled.h5`
    ${smallHeading};
    color: ${(props) => (props.color ? props.color : '#001d1b')};
`;
export const RegularText = styled.p`
    font-size:${Theme.$regularText}
    line-height:19px;
    color: ${(props) => (props.color ? props.color : '#001d1b')};
`;
export const SmallText = styled.p`
    ${smallText};
    color: ${(props) => (props.color ? props.color : '#001d1b')};
`;
// Button Declaration

const myButtonOne = css`
    ${globalBg};
    padding: 14px 0;
    border: 0;
    border-radius: 32px;
`;
const myButtonTwo = css`
    ${globalBg};
    padding: 12px 0;
    border: 0;
    border-radius: 32px;
`;
const myButtonThree = css`
    padding: 12px 0;
    border: 0;
    border-radius: 32px;
`;
export const MyButton = styled.button`
    ${globalBg};
    padding: 6px 30px 10px 30px;
    border: 0;
    border-radius: 32px;
`;
export const ButtonLarge = styled.button`
    ${myButtonOne};
    width: 100%;
`;

export const HappyImg = styled.div`
    img {
        @media ${Theme.$tablet} {
        }
    }
`;
export const LoginFormColumn = styled.div`
    @media ${Theme.$mobileS} {
        ${Col12}
        padding: 40px 20px;
    }
    @media ${Theme.$tablet} {
        ${Col4}
    }
`;

export const UserFormWrapper = styled.div`
    max-width: 400px;
    width: 100%;
`;

export const LoginLogo = styled.div`
    max-width: 212px;
    @media ${Theme.$mobileS} {
        margin-bottom: 20px;
    }
    @media ${Theme.$tablet} {
        margin-bottom: 40px;
    }
    @media ${Theme.$desktop} {
        margin-bottom: 60px;
    }
`;
export const LoginForm = styled.div`
    @media ${Theme.$mobileS} {
        padding-top: 40px;
    }
    @media ${Theme.$tablet} {
        padding-top: 60px;
    }
    @media ${Theme.$desktop} {
        padding-top: 100px;
    }

    button {
        margin-top: 20px;
    }
`;

export const LoginFormInput = styled.div`
    @media ${Theme.$mobileS} {
        margin-bottom: 40px;
    }

    @media ${Theme.$desktop} {
        margin-bottom: 60px;
    }
`;
export const InputGroup = styled.div`
    margin-bottom: 20px;
`;

export const Input = styled.input`
    width: 100%;
    border: 0;
    color: ${Theme.$lightBlackColor};
    border-bottom: 1px solid ${Theme.$lightBlackColor};
    padding: 5px 0;
`;

export const ResetPassword = styled.div`
    text-align: right;

    a {
        color: ${Theme.$secondaryColor};
    }
`;

export const PasswordEditPage = styled.div`
    ${BgImgstyle};
    background: url(${happyPeopleBg});
    height: 100vh;
    padding: 20px;
`;
export const PasswordEditBox = styled.div`
    max-width: 760px;
    width: 100%;
    border-radius: 8px;
    background: ${Theme.$whiteColor};
`;

export const PasswordEditLogo = styled.div`
    max-width: 150px;
    margin: 0 auto;

    @media ${Theme.$mobileS} {
        padding: 20px 0;
    }
    @media ${Theme.$desktop} {
        padding: 55px 0 20px 0;
    }
`;
export const PasswordFormBox = styled.div`
    max-width: 400px;
    width: 100%;
    margin: 0 auto;

    @media ${Theme.$mobileS} {
        padding: 20px 0;
    }
    @media ${Theme.$tablet} {
        padding: 30px 0;
    }
    @media ${Theme.$desktop} {
        padding: 55px 0;
    }
    &#fp-formbox {
        @media ${Theme.$desktop} {
            padding: 95px 0 55px 0;
        }
    }
`;
export const PasswordForm = styled.div`
    padding: 20px 0;
    &#fp-password-form {
        @media ${Theme.$desktop} {
            padding: 30px 0 20px 0;
        }
    }
`;
export const PasswordEditHeading = styled.h4`
    ${mediumHeading};
    color: ${Theme.$BlackColorshade};
    &#reset-link-heading {
        margin-bottom: 30px;
    }
`;
export const PasswordText = styled.p`
    ${smallText};
    color: ${Theme.$TextColorOne};
`;
export const FormInput = styled.div`
    @media ${Theme.$mobileS} {
        margin-bottom: 20px;
    }

    @media ${Theme.$desktop} {
        margin-bottom: 40px;
    }
`;
export const PasswordButtonRow = styled.div`
    text-align: center;
    @media ${Theme.$mobileS} {
        padding: 20px 0;
    }
    @media ${Theme.$tablet} {
        padding: 30px 0;
    }
    @media ${Theme.$desktop} {
        padding: 50px 0;
    }
    &#fp-submit-row {
        @media ${Theme.$desktop} {
            padding: 70px 0 50px 0;
        }
    }
`;
const PasswordButton = css`
    max-width: 200px;
    width: 100%;
`;
export const PasswordSubmitButton = styled.button`
    ${myButtonTwo};
    ${PasswordButton};
`;
export const PasswordCancelButton = styled.button`
    ${myButtonThree};
    ${PasswordButton};
    background: none;
    color: ${Theme.$BrownColor};

    &:hover {
        background: ${Theme.$secondaryColor};
        color: ${Theme.$whiteColor};
    }
`;
export const ResetLinkBox = styled.div`
    max-width: 400px;
    width: 100%;
    margin: 0 auto;

    @media ${Theme.$mobileS} {
        padding: 40px 0;
    }
    @media ${Theme.$tablet} {
        padding: 80px 0;
    }
    @media ${Theme.$desktop} {
        padding: 150px 0 55px 0;
    }
`;
export const ResendLink = styled.div`
    @media ${Theme.$mobileS} {
        padding: 20px 0;
    }
    @media ${Theme.$tablet} {
        padding: 30px 0;
    }
    @media ${Theme.$desktop} {
        padding: 155px 0 40px 0;
    }
    a {
        color: ${Theme.$colorGreenShade};
    }
`;

export const HomeBox = styled.div`
    background: #f5f5f5;
    width: 100%;
    max-width: 600px;
    margin: 0 auto;
    box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.5);
    border-radius: 10px;
    @media ${Theme.$mobileS} {
        padding: 50px 0;
    }
    @media ${Theme.$tablet} {
        padding: 70px 0;
    }
    @media ${Theme.$desktop} {
        padding: 100px 0;
    }
    &#homepage-text {
        margin-bottom: 30px;
    }
`;
export const HomePageButtonRow = styled.div`
    &#hp-button-row {
        button {
            margin-right: 10px;
        }
    }
`;
