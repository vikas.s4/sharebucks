import axios from 'axios';

import { BASE_URL, NON_AUTHORIZATION_APIS } from '../components/constants/apiConstant';

const axiosInstance = axios.create({
    baseURL: BASE_URL,
});
axiosInstance.interceptors.request.use((request) => {
    let token = localStorage.getItem('token');

    if (!NON_AUTHORIZATION_APIS.includes(request.url) || request.method === 'get') {
        token = token || '';
        request.headers.Authorization = `Bearer ${token}`;
    }
    return request;
});

export default axiosInstance;
