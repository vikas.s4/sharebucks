import React from 'react';
import { string, func } from 'prop-types';

import { Input } from '../../theme/css/style';

const CustomInput = ({ name, type, placeholder, value, onChange }) => (
    <Input name={name} type={type} placeholder={placeholder} value={value} onChange={onChange} />
);

export default CustomInput;

CustomInput.defaultProps = {
    onChange: () => {},
};
CustomInput.propTypes = {
    name: string.isRequired,
    type: string.isRequired,
    placeholder: string.isRequired,
    value: string.isRequired,
    onChange: func,
};
