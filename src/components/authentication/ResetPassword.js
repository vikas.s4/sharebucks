import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { resetpassword } from '../../redux/actions/userState';
import CustomInput from '../common/CustomInput';
import SiteLogo from '../../theme/images/login-logo.png';
import {
    PasswordEditPage,
    PasswordEditBox,
    PasswordEditLogo,
    PasswordFormBox,
    UserFormWrapper,
    PasswordEditHeading,
    PasswordText,
    PasswordForm,
    FormInput,
    InputGroup,
    PasswordButtonRow,
    PasswordSubmitButton,
    PasswordCancelButton,
    FormError,
} from '../../theme/css/style';

function ResetPassword() {
    const history = useHistory();
    const [formState, setFormState] = useState({
        password: '',
        confirmPassword: '',
    });

    const dispatch = useDispatch();
    const { error } = useSelector((state) => state.userReducer);

    const handleInputChange = (e) => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmitClick = (e) => {
        e.preventDefault();
        const sendFormData = {
            password: formState.password,
            confirm_password: formState.confirmPassword,
        };

        dispatch(resetpassword(sendFormData, history));
    };

    return (
        <PasswordEditPage
            id="reset-password"
            className="d-flex justify-content-center align-items-center"
        >
            <PasswordEditBox>
                <PasswordEditLogo className="text-center">
                    <img src={SiteLogo} alt="site logo" />
                </PasswordEditLogo>

                <PasswordFormBox>
                    <UserFormWrapper>
                        <PasswordEditHeading>Reset Password</PasswordEditHeading>
                        <PasswordText>Please enter a new password below</PasswordText>
                        <PasswordForm>
                            <form>
                                <FormInput>
                                    <InputGroup>
                                        <CustomInput
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                            value={formState.password}
                                            onChange={handleInputChange}
                                        />
                                    </InputGroup>
                                    {error ? (
                                        <FormError>
                                            <p>{error.password}</p>
                                        </FormError>
                                    ) : (
                                        ''
                                    )}
                                </FormInput>

                                <FormInput>
                                    <InputGroup>
                                        <CustomInput
                                            name="confirmPassword"
                                            type="password"
                                            placeholder="Confirm Password"
                                            value={formState.confirmPassword}
                                            onChange={handleInputChange}
                                        />
                                    </InputGroup>
                                    {error ? (
                                        <FormError>
                                            <p>{error.confirm_password}</p>
                                        </FormError>
                                    ) : (
                                        ''
                                    )}
                                </FormInput>

                                <PasswordButtonRow>
                                    <PasswordCancelButton type="submit">
                                        Cancel
                                    </PasswordCancelButton>
                                    <PasswordSubmitButton type="submit" onClick={handleSubmitClick}>
                                        Submit
                                    </PasswordSubmitButton>
                                </PasswordButtonRow>
                            </form>
                        </PasswordForm>
                    </UserFormWrapper>
                </PasswordFormBox>
            </PasswordEditBox>
        </PasswordEditPage>
    );
}

export default ResetPassword;
