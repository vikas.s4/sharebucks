import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { userLoginRequest } from '../../redux/actions/userState';
import CustomInput from '../common/CustomInput';
import SiteLogo from '../../theme/images/login-logo.png';
import HappyPeopleImg from '../../theme/images/happy-people.png';
import {
    MyRow,
    LoginImgcolumn,
    HappyImg,
    LoginFormColumn,
    UserFormWrapper,
    LoginLogo,
    HeadingH1,
    HeadingH5,
    LoginForm,
    LoginFormInput,
    InputGroup,
    ButtonLarge,
    FormError,
    ResetPassword,
} from '../../theme/css/style';
import { PATH_FORGOT_PASSWORD } from '../constants/routeConstant';

function Login() {
    const history = useHistory();
    const [formState, setFormState] = useState({
        email: '',
        password: '',
    });
    const dispatch = useDispatch();
    const { error } = useSelector((state) => state.userReducer);

    const handleInputChange = (e) => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmitClick = (e) => {
        e.preventDefault();

        const sendFormData = {
            username: formState.email,
            password: formState.password,
        };

        dispatch(userLoginRequest(sendFormData, history));
    };

    return (
        <div className="login-box">
            <MyRow>
                <LoginImgcolumn>
                    <HappyImg>
                        <img src={HappyPeopleImg} alt="Happy People" />
                    </HappyImg>
                </LoginImgcolumn>
                <LoginFormColumn className="d-flex justify-content-center align-items-center">
                    <UserFormWrapper>
                        <LoginLogo>
                            <img src={SiteLogo} alt="site logo" />
                        </LoginLogo>
                        <HeadingH1>Welcome Back</HeadingH1>
                        <HeadingH5>Please sign into your account</HeadingH5>
                        <LoginForm>
                            <form>
                                <LoginFormInput>
                                    <InputGroup>
                                        <CustomInput
                                            name="email"
                                            type="email"
                                            placeholder="Email Address"
                                            value={formState.email}
                                            onChange={handleInputChange}
                                        />
                                    </InputGroup>
                                </LoginFormInput>

                                <LoginFormInput>
                                    <InputGroup>
                                        <CustomInput
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                            value={formState.password}
                                            onChange={handleInputChange}
                                        />
                                    </InputGroup>
                                    <ResetPassword>
                                        <Link to={PATH_FORGOT_PASSWORD}>Forgot Password?</Link>
                                    </ResetPassword>
                                </LoginFormInput>
                                {error ? (
                                    <FormError>
                                        <p>{error.error}</p>
                                    </FormError>
                                ) : (
                                    ''
                                )}
                                <ButtonLarge type="submit" onClick={handleSubmitClick}>
                                    Sign In
                                </ButtonLarge>
                            </form>
                        </LoginForm>
                    </UserFormWrapper>
                </LoginFormColumn>
            </MyRow>
        </div>
    );
}

export default Login;
