import React from 'react';
import { Link } from 'react-router-dom';

import SiteLogo from '../../theme/images/login-logo.png';
import {
    PasswordEditPage,
    PasswordEditBox,
    PasswordEditLogo,
    ResetLinkBox,
    UserFormWrapper,
    PasswordEditHeading,
    PasswordText,
    ResendLink,
} from '../../theme/css/style';
import { PATH_FORGOT_PASSWORD } from '../constants/routeConstant';

function ResetLinkSent() {
    return (
        <PasswordEditPage
            id="forgot-password"
            className="d-flex justify-content-center align-items-center"
        >
            <PasswordEditBox>
                <PasswordEditLogo className="text-center">
                    <img src={SiteLogo} alt="site logo" />
                </PasswordEditLogo>

                <ResetLinkBox>
                    <UserFormWrapper className="text-center">
                        <PasswordEditHeading id="reset-link-heading">
                            Check Your Email
                        </PasswordEditHeading>
                        <PasswordText>
                            We sent you an email with a link to reset your password.
                        </PasswordText>
                        <ResendLink>
                            <Link to={PATH_FORGOT_PASSWORD}>Resend Email</Link>
                        </ResendLink>
                    </UserFormWrapper>
                </ResetLinkBox>
            </PasswordEditBox>
        </PasswordEditPage>
    );
}

export default ResetLinkSent;
