import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { forgotpassword } from '../../redux/actions/userState';
import CustomInput from '../common/CustomInput';
import SiteLogo from '../../theme/images/login-logo.png';
import {
    PasswordEditPage,
    PasswordEditBox,
    PasswordEditLogo,
    PasswordFormBox,
    UserFormWrapper,
    PasswordEditHeading,
    PasswordText,
    PasswordForm,
    FormInput,
    InputGroup,
    PasswordButtonRow,
    PasswordSubmitButton,
    PasswordCancelButton,
    FormError,
} from '../../theme/css/style';

function ForgotPassword() {
    const history = useHistory();
    const [formState, setFormState] = useState({
        email: '',
    });
    const dispatch = useDispatch();
    const { error } = useSelector((state) => state.userReducer);

    const handleInputChange = (e) => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmitClick = (e) => {
        e.preventDefault();
        const sendFormData = {
            email: formState.email,
        };

        dispatch(forgotpassword(sendFormData, history));
    };

    return (
        <PasswordEditPage
            id="forgot-password"
            className="d-flex justify-content-center align-items-center"
        >
            <PasswordEditBox>
                <PasswordEditLogo className="text-center">
                    <img src={SiteLogo} alt="site logo" />
                </PasswordEditLogo>

                <PasswordFormBox id="fp-formbox">
                    <UserFormWrapper>
                        <PasswordEditHeading>Forgot Password</PasswordEditHeading>
                        <PasswordText>
                            We’ll send you an email with a link to reset your password.
                        </PasswordText>
                        <PasswordForm id="fp-password-form">
                            <form>
                                <FormInput>
                                    <InputGroup>
                                        <CustomInput
                                            name="email"
                                            type="email"
                                            placeholder="Email Address"
                                            value={formState.email}
                                            onChange={handleInputChange}
                                        />
                                    </InputGroup>
                                    {error ? (
                                        <FormError>
                                            <p>{error.email}</p>
                                        </FormError>
                                    ) : (
                                        ''
                                    )}
                                </FormInput>

                                <PasswordButtonRow id="fp-submit-row">
                                    <PasswordCancelButton type="submit">
                                        Cancel
                                    </PasswordCancelButton>
                                    <PasswordSubmitButton type="submit" onClick={handleSubmitClick}>
                                        Submit
                                    </PasswordSubmitButton>
                                </PasswordButtonRow>
                            </form>
                        </PasswordForm>
                    </UserFormWrapper>
                </PasswordFormBox>
            </PasswordEditBox>
        </PasswordEditPage>
    );
}

export default ForgotPassword;
