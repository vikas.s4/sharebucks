// authentication path
export const PATH_LOGIN = '/login';
export const PATH_FORGOT_PASSWORD = '/forgot-password';
export const PATH_RESERT_PASSWORD = '/reset-password';
export const PATH_RESET_LINK_SEND = '/reset-link-sent';

// dashboard path
export const PATH_DASHBOARD = '/dashboard';

// other path
export const PATH_HOME = '/';
