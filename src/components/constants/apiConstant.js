import { API_VERSION } from './authConstant';
// API required
export const BASE_URL = 'https://crm-api-stage.sharebucks.io/api';
export const LOGIN_URL = `${BASE_URL}/auth/token/`;
export const LOGOUT_URL = `${BASE_URL}/auth/revoke-token/`;
export const FORGOT_PASSOWORD_URL = `${BASE_URL}/${API_VERSION}/user/forgot-password/`;
export const RESET_PASSSOWORD_URL = `${BASE_URL}/${API_VERSION}/user/reset-password/`;
export const USERME_URL = `${BASE_URL}/${API_VERSION}/user/me/`;

export const NON_AUTHORIZATION_APIS = [
    LOGIN_URL,
    LOGOUT_URL,
    FORGOT_PASSOWORD_URL,
    RESET_PASSSOWORD_URL,
];
