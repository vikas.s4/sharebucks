import React from 'react';
import { useHistory } from 'react-router-dom';

import { useDispatch } from 'react-redux';

import { logout } from '../redux/actions/userState';
import { PATH_DASHBOARD, PATH_LOGIN } from './constants/routeConstant';
import { HeadingH4, HomeBox, HomePageButtonRow, MyButton } from '../theme/css/style';

function Home() {
    const history = useHistory();
    const accessToken = localStorage.getItem('token');
    const currentUser = JSON.parse(localStorage.getItem('user'));

    const dispatch = useDispatch();

    const handleLogIn = (e) => {
        e.preventDefault();

        history.push(PATH_LOGIN);
    };
    const handleDashboard = (e) => {
        e.preventDefault();

        history.push(PATH_DASHBOARD);
    };
    const handleLogOut = (e) => {
        e.preventDefault();
        dispatch(logout(accessToken, history));
    };

    return (
        <div id="Homepage">
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="d-flex h-100">
                            <HomeBox>
                                <HeadingH4 className="homepage-text">
                                    Welcome To SHAREBUCKS
                                </HeadingH4>

                                {currentUser ? (
                                    <>
                                        <h5 className="homepage-text">
                                            You are Log In as {currentUser.user.email}
                                        </h5>
                                        <HomePageButtonRow id="hp-button-row">
                                            <MyButton type="button" onClick={handleDashboard}>
                                                Dashboard
                                            </MyButton>
                                            <MyButton type="button" onClick={handleLogOut}>
                                                Logout
                                            </MyButton>
                                        </HomePageButtonRow>
                                    </>
                                ) : (
                                    <MyButton type="button" onClick={handleLogIn}>
                                        Login
                                    </MyButton>
                                )}
                            </HomeBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;
