import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { useDispatch } from 'react-redux';

import { userme, logout } from '../redux/actions/userState';
import { HeadingH4, HomeBox, MyButton } from '../theme/css/style';

function Dashboard() {
    const history = useHistory();
    const dispatch = useDispatch();
    const accessToken = localStorage.getItem('token');

    const currentUser = JSON.parse(localStorage.getItem('user'));

    const handleLogOut = (e) => {
        e.preventDefault();
        dispatch(logout(accessToken, history));
    };

    useEffect(() => {
        dispatch(userme(history));
    }, [dispatch, history]);

    return (
        <div id="Homepage">
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="d-flex h-100">
                            <HomeBox>
                                <HeadingH4 className="homepage-text">
                                    Welcome To SHAREBUCKS
                                </HeadingH4>

                                <h5 className="homepage-text">
                                    You are Log In as {currentUser.user.email}
                                </h5>
                                <MyButton type="button" onClick={handleLogOut}>
                                    Logout
                                </MyButton>
                            </HomeBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;
