import axiosInstance from '../../api/interceptor';
import {
    CLIENT_ID,
    CLIENT_SECRET,
    GRANT_TYPE,
    CODE,
} from '../../components/constants/authConstant';
import {
    LOGIN_URL,
    LOGOUT_URL,
    FORGOT_PASSOWORD_URL,
    RESET_PASSSOWORD_URL,
    USERME_URL,
} from '../../components/constants/apiConstant';
import * as actionTypes from './actionTypes';
import {
    PATH_HOME,
    PATH_LOGIN,
    PATH_RESET_LINK_SEND,
} from '../../components/constants/routeConstant';

export const userLoginRequest = (userData, history) => {
    const body = {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        grant_type: GRANT_TYPE,
        username: userData.username,
        password: userData.password,
    };

    return (dispatch) => {
        dispatch({
            type: actionTypes.LOGIN_REQUEST,
        });

        axiosInstance
            .post(LOGIN_URL, body)
            .then((response) => {
                localStorage.setItem('token', response.data.access_info.access_token);
                localStorage.setItem('user', JSON.stringify(response.data));
                localStorage.setItem('isAuthenticated', 'true');
                dispatch({
                    type: actionTypes.LOGIN_SUCCESS,
                    token: response.data.access_info.access_token,
                    user: response.data,
                });
                history.push(PATH_HOME);
            })
            .catch((error) => {
                dispatch({
                    type: actionTypes.LOGIN_FAILURE,
                    error: error.response.data,
                });
            });
    };
};

export const logout = (accessToken, history) => {
    const body = {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        token: accessToken,
    };

    return (dispatch) => {
        axiosInstance
            .post(LOGOUT_URL, body)
            .then(() => {
                localStorage.clear();
                dispatch({
                    type: actionTypes.LOGOUT_SUCCESS,
                });
                history.push(PATH_LOGIN);
            })
            .catch(() => {
                dispatch({
                    type: actionTypes.LOGOUT_FAILURE,
                });
            });
    };
};

export const forgotpassword = (userData, history) => {
    const body = {
        email: userData.email,
    };
    return (dispatch) => {
        axiosInstance
            .post(FORGOT_PASSOWORD_URL, body)
            .then(() => {
                dispatch({
                    type: actionTypes.FORGOT_PASSWORD_SUCCESS,
                });
                history.push(PATH_RESET_LINK_SEND);
            })
            .catch((error) => {
                dispatch({
                    type: actionTypes.FORGOT_PASSWORD_FAILURE,
                    error: error.response.data,
                });
            });
    };
};

export const resetpassword = (userData, history) => {
    const body = {
        code: CODE,
        password: userData.password,
        confirm_password: userData.confirm_password,
    };
    return (dispatch) => {
        axiosInstance
            .post(RESET_PASSSOWORD_URL, body)
            .then(() => {
                dispatch({
                    type: actionTypes.RESET_PASSWORD_SUCCESS,
                });
                history.push(PATH_LOGIN);
            })
            .catch((error) => {
                dispatch({
                    type: actionTypes.RESET_PASSWORD_FAILURE,
                    error: error.response.data,
                });
            });
    };
};

export const userme = (history) => (dispatch) => {
    axiosInstance
        .get(USERME_URL)
        .then(() => {
            dispatch({
                type: actionTypes.AUTHENTICATION_SUCCESS,
            });
        })
        .catch((error) => {
            localStorage.clear();
            dispatch({
                type: actionTypes.AUTHENTICATION_FAILURE,
                error: error.response.data,
            });
            history.push(PATH_LOGIN);
        });
};
