import * as actionTypes from '../actions/actionTypes';

const INITIAL_STATE = {
    token: null,
    user: {},
    error: null,
    loading: false,
    isLoggedIn: false,
};
const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                user: action.user,
                token: action.token,
                loading: false,
                error: null,
                isLoggedIn: true,
            };

        case actionTypes.LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
                user: null,
                token: null,
                isLoggedIn: false,
                error: action.error,
            };

        case actionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
                token: null,
                error: null,
            };
        case actionTypes.LOGOUT_FAILURE:
            return {
                ...state,
                isLoggedIn: true,
                error: action.error,
                user: action.user,
                token: action.token,
            };
        case actionTypes.AUTHENTICATION_SUCCESS:
            return {
                ...state,
                user: action.user,
                token: action.token,
                loading: false,
                error: null,
                isLoggedIn: true,
            };

        case actionTypes.AUTHENTICATION_FAILURE:
            return {
                ...state,
                loading: false,
                user: null,
                token: null,
                isLoggedIn: false,
                error: action.error,
            };
        case actionTypes.FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
            };
        case actionTypes.FORGOT_PASSWORD_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        case actionTypes.RESET_PASSWORD_SUCCESS:
            return {
                ...state,
            };
        case actionTypes.RESET_PASSWORD_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};

export default userReducer;
